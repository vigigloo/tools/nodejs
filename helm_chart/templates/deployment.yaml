{{- $envVars := include "common.env.transformDict" .Values.envVars -}}
{{- $fullName := include "common.fullname" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  {{- with .Values.strategy }}
  strategy:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "common.selectorLabels" . | nindent 6 }}
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        {{- include "common.selectorLabels" . | nindent 8 }}
    spec:
      {{- if $.Values.image.credentials }}
      imagePullSecrets:
        - name: {{ include "common.secret.dockerconfigjson.name" (dict "fullname" $fullName "imageCredentials" $.Values.image.credentials) }}
      {{- end}}
      serviceAccountName: {{ include "common.serviceAccountName" . }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          {{- if $envVars}}
          env:
            {{- $envVars | indent 12 }}
          {{- end }}
          ports:
            - name: http
              containerPort: {{ .Values.service.targetPort }}
              protocol: TCP
            {{- if .Values.monitoring.exporter.enabled }}
            - name: metrics
              containerPort: {{ .Values.monitoring.exporter.port }}
              protocol: TCP
            {{- end }}
          {{- if .Values.probes.liveness }}
          livenessProbe:
            {{- include "common.probes.abstract" (merge .Values.probes.liveness (dict "targetPort" .Values.service.targetPort )) | nindent 12 }}
          {{- end }}
          {{- if .Values.probes.readiness }}
          readinessProbe:
            {{- include "common.probes.abstract" (merge .Values.probes.readiness (dict "targetPort" .Values.service.targetPort )) | nindent 12 }}
          {{- end }}
          {{- if .Values.probes.startup }}
          startupProbe:
            {{- include "common.probes.abstract" (merge .Values.probes.startup (dict "targetPort" .Values.service.targetPort )) | nindent 12 }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          {{- if .Values.persistence }}
          volumeMounts:
            {{- range $name, $volume := .Values.persistence }}
            - name: "{{ $name }}"
              mountPath: "{{ $volume.mountPath }}"
            {{- end }}
          {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if .Values.persistence }}
      volumes:
        {{- range $name, $volume := .Values.persistence }}
        - name: "{{ $name }}"
          persistentVolumeClaim:
            claimName: "{{ $fullName }}-{{ $name }}"
        {{- end }}
      {{- end }}
